from rest_framework.serializers import ModelSerializer
from cidade.models import Cidade
from estado.api.serializers import EstadoSerializer

class CidadeSerializer(ModelSerializer):
    #estado = EstadoSerializer()

    class Meta:
        model = Cidade
        fields = ['id', 'descricao', 'estado_id']
