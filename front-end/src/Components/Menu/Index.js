import { Component } from "react";
import { Link } from 'react-router-dom'

class Menu extends Component{
    render(){
        return(
          <nav id="sidebarMenu" className="col-md-2 d-none d-md-block bg-light sidebar">
              <div className="sidebar-sticky">
                 <ul className="nav flex-column">
                     <li className="nav-item">
                         <Link to={'/'} className="nav-link">Dashboard</Link>
                     </li>
                     <li className="nav-item">
                         <Link to={'/cidades'} className="nav-link">Cidades</Link>
                     </li>
                     <li className="nav-item">
                         <Link to={'/estados'} className="nav-link">Estados</Link>
                     </li>
                 </ul>
             </div>
          </nav>
        )
    }
}

export default Menu;