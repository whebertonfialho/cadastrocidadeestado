import { Button, Modal, Row } from 'react-bootstrap'


export default function CidadeModal({ show, close, detalhe }){
  
  return(
     <Modal show={show} onHide={close} animation={false}>
       <Modal.Header closeButton>
         <Modal.Title>Detalhes da Cidade</Modal.Title>
       </Modal.Header>
       <Modal.Body>
         <Row>
           <span>
             <b>Código:</b> <i>{detalhe ? detalhe.id : ""}</i>
           </span>
         </Row>
         <Row>
          <span>
            <b>Descrição:</b> <i>{detalhe ? detalhe.descricao : ""}</i>
          </span>
         </Row>
       </Modal.Body>
       <Modal.Footer>
         <Button variant="secondary" onClick={ close }>
           Sair
         </Button>
       </Modal.Footer>
     </Modal>
  )
}