import { useState, useEffect } from 'react'
import { FiSearch } from 'react-icons/fi';

import api from '../../Services/api'
import EstadoModal from '../Estado/Modal'

export default function EstadoList(){

    const [estados, setEstados] = useState([])
    const [detalhe, setDetalhe] = useState();
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  
    useEffect(()=> {
      async function buscarEstados(){
        await api.get('/estado')
                .then( (response) => { 
                  setEstados(response.data) 
                })
                .catch((err)=>{ 
                  console.log('Deu algum erro: ', err); 
              })
      } 

      buscarEstados();

      return () => { }
    }, []);

    function togglePostModal(item){
      handleShow();
      setDetalhe(item);
    }

    return(
      <div className="col-sm-12"> 
        <div className="col-sm-12">
            <h3 className="h3 border-bottom">Estados</h3>
            <br></br>
            <table className="table table-hover table-sm">
                <thead>
                    <tr>
                    <th scope="col" width="10%">#</th>
                    <th scope="col">Descrição</th>
                    <th scope="col" width="10%">Opção</th>
                    </tr>
                </thead>
                <tbody>
                  {estados.map((item, index)=>{
                    return(
                      <tr key={index}>
                        <td data-label="Id">{item.id}</td>
                        <td data-label="Descrição">{item.descricao}</td>
                        <td data-label="Opção">
                          <button className="btn btn-primary btn-sm" style={{backgroundColor: '#3583f6' }} onClick={ () => togglePostModal(item) }>
                            <FiSearch color="#FFF" size={17} />
                          </button>
                          {/* <Link className="btn btn-warning btn-sm" style={{backgroundColor: '#F6a935' }} to={`/new/${item.id}`} >
                            <FiEdit2 color="#FFF" size={17} />
                          </Link> */}
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
            </table>
        </div>
        <EstadoModal show={ show } close={ handleClose } detalhe={ detalhe } />
      </div>
    )
}