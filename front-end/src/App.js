import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import { BrowserRouter } from 'react-router-dom';

import AuthProvider from './Contexts/Auth';
import Routes from './Routes/Index';
import Header from './Components/Header/Index';
import Menu from './Components/Menu/Index';

function App() {
  return (
    <div className="App">
      <Header />
      <AuthProvider>
        <BrowserRouter>
        <ToastContainer autoClose={3000} />
          <div className="container-fluid">
            <div className="row">
              <Menu />
                <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                  <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
                    <Routes/>
                  </div>
                </main>
            </div>
          </div>
        </BrowserRouter>
      </AuthProvider>
    </div>


    // <AuthProvider>
    //   <BrowserRouter>
    //     <ToastContainer autoClose={3000} />
    //     <Routes/>
    //   </BrowserRouter>
    // </AuthProvider>
  );
}

export default App;
