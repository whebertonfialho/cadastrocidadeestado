import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '../Pages/SignIn';
import SignUp from '../Pages/SignUp';

import Dashboard from '../Pages/Dashboard/index';
import CidadeList from '../Pages/Cidade/List';
import EstadosList from '../Pages/Estado/List';

export default function Routes(){
  return(
    <Switch>
      <Route exact path="/" component={SignIn} />
      <Route exact path="/register" component={SignUp} />
      <Route exact path="/dashboard" component={ Dashboard } isPrivate />
      <Route exact path="/cidades" component={ CidadeList } isPrivate />
      <Route exact path="/estados" component={ EstadosList } isPrivate />
    </Switch>
  )
}