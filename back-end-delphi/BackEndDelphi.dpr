program BackEndDelphi;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.JSON,
  Horse,
  Horse.Jhonson,
  Horse.CORS,
  ServerReact.Model.Connection in 'src\Model\ServerReact.Model.Connection.pas',
  ServerReact.Model.DAOGeneric in 'src\Model\ServerReact.Model.DAOGeneric.pas',
  ServerReact.Model.Entidades.Cidade in 'src\Model\Entidades\ServerReact.Model.Entidades.Cidade.pas',
  ServerReact.Model.Entidades.Estado in 'src\Model\Entidades\ServerReact.Model.Entidades.Estado.pas',
  ServerReact.Controller.Cidade in 'src\Controller\ServerReact.Controller.Cidade.pas',
  ServerReact.Controller.Estado in 'src\Controller\ServerReact.Controller.Estado.pas';

var
  App : THorse;

begin
  App := THorse.Create(9000);
  App.Use(Jhonson);
  App.Use(CORS);

  THorse.Get('/',
    procedure(Req: THorseRequest; Res: THorseResponse; Next: TProc)
    begin
      Res.Send('<h2>It'+ #39 + 's Work!</h2>');
    end);

  ServerReact.Controller.Cidade.Registry(App);
  ServerReact.Controller.Estado.Registry(App);

  App.Start;
end.
