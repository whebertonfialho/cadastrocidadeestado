unit ServerReact.Model.Entidades.Cidade;

interface

uses
  SimpleAttributes;

type
  [Tabela('tab_cidade')]
  TCidade = class
    private
    FDescricao: String;
    FEstadoId: Integer;
    FId: Integer;
    procedure SetDescricao(const Value: String);
    procedure SetEstadoId(const Value: Integer);
    procedure SetId(const Value: Integer);
    public
      constructor Create;
      destructor Destroy; override;
    published
      [Campo('id'), PK]
      property Id : Integer read FId write SetId;
      [Campo('descricao')]
      property Descricao : String read FDescricao write SetDescricao;
      [Campo('estado_id')]
      property EstadoId : Integer read FEstadoId write SetEstadoId;
  end;

implementation

{ TCidade }

constructor TCidade.Create;
begin

end;

destructor TCidade.Destroy;
begin
  inherited;
end;

procedure TCidade.SetDescricao(const Value: String);
begin
  FDescricao := Value;
end;

procedure TCidade.SetEstadoId(const Value: Integer);
begin
  FEstadoId := Value;
end;

procedure TCidade.SetId(const Value: Integer);
begin
  FId := Value;
end;

end.
