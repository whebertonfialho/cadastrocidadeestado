unit ServerReact.Model.Entidades.Estado;

interface

uses
  SimpleAttributes;

type
  [Tabela('tab_estado')]
  TEstado = class
    private
    FDescricao: String;
    FId: Integer;
    procedure SetDescricao(const Value: String);
    procedure SetId(const Value: Integer);
    public
      constructor Create;
      destructor Destroy; override;
    published
      [Campo('id'), PK]
      property Id : Integer read FId write SetId;
      [Campo('descricao')]
      property Descricao : String read FDescricao write SetDescricao;
  end;

implementation

{ TEstado }

constructor TEstado.Create;
begin

end;

destructor TEstado.Destroy;
begin

  inherited;
end;

procedure TEstado.SetDescricao(const Value: String);
begin
  FDescricao := Value;
end;

procedure TEstado.SetId(const Value: Integer);
begin
  FId := Value;
end;

end.
