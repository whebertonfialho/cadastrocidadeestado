unit ServerReact.Model.Connection;

interface

uses
  FireDac.Stan.Intf,
  FireDac.Stan.Option,
  FireDac.Stan.Error,
  FireDac.UI.Intf,
  FireDac.Phys.Intf,
  FireDac.Stan.Def,
  FireDac.Stan.Pool,
  FireDac.Stan.Async,
  FireDac.Phys,
  FireDac.VCLUI.Wait,
  Data.DB,
  FireDAC.Comp.Client,
  FireDAC.DApt,
  FireDAC.Phys.MySQLDef,
  FireDAC.Phys.MySQL;

var
  FConn : TFDConnection;

function Connected : TFDConnection;
procedure Disconnected;

implementation

function Connected : TFDConnection;
begin;
  FConn := TFDConnection.Create(nil);
  FConn.Params.DriverID := 'MySQL';
  FConn.Params.Database := 'tabela_cidade';
  FConn.Params.UserName := 'root';
  FConn.Params.Password := 'root';
  FConn.Params.Add('Port=3306');
  FConn.Params.Add('Server=127.0.0.1');
  FConn.Connected;
  Result := FConn;
end;

procedure Disconnected;
begin;
  if Assigned(FConn) then
  begin;
    FConn.Connected := False;
    FConn.Free;
  end;
end;

end.
